<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//----------------------------------------- User Section ---------------------------------------------


//User Authentication Section
Route::post('/user/sms/send', "SmsController@CheckSendSmsData");
Route::post('/user/login', "AuthController@UserLogin");

Route::middleware("access.level:1")->group(function () {

//User Profile Section
    Route::post('/user/profile/edit', "UserController@CheckEditProfileData");
    Route::get('/user/profile', "UserController@GetUserProfile");

});
