<?php

namespace App\Http\Middleware;

use App\Models\Admin;
use App\Models\Courier;
use App\Models\Marketer;
use App\Models\Seller;
use App\Models\User;
use Closure;

class AccessLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard)
    {
        if ($request->hasHeader("Authorization")) {
            $token = $request->header("Authorization");
            switch ($guard) {
                case 1:
                    $user = User::where("token", $token)->first();
                    if ($user != null) {
                        $request = $request->merge(["UserType" => 1]);
                        return $next($request)
                            ->header('Access-Control-Allow-Origin', '*')
                            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
                    } else {
                        $message = "خطا در احراز هویت کاربر ";
                        return response()->json(["status" => false, "message" => $message], 403);
                    }
                    break;
                case 2:
                    $user = Seller::where("token", $token)->first();
                    if ($user != null) {
                        $request = $request->merge(["UserType" => 2]);
                        return $next($request)
                            ->header('Access-Control-Allow-Origin', '*')
                            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
                    } else {
                        $message = "خطا در احراز هویت فروشنده ";
                        return response()->json(["status" => false, "message" => $message], 403);
                    }
                    break;
                case 3:
                    $admin = Admin::where("token", $token)->first();
                    if ($admin != null) {
                        $request = $request->merge(["UserType" => 3]);
                        return $next($request)
                            ->header('Access-Control-Allow-Origin', '*')
                            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
                    } else {
                        $message = "خطا در احراز هویت مدیریت ";
                        return response()->json(["status" => false, "message" => $message], 403);
                    }
                    break;
                case 4:
                    $marketer = Marketer::where("token", $token)->first();
                    if ($marketer != null) {
                        $request = $request->merge(["UserType" => 4]);
                        return $next($request);
                    } else {
                        $message = "خطا در احراز هویت بازاریاب ";
                        return response()->json(["status" => false, "message" => $message], 403);
                    }
                    break;
                case 5:
                    $courier = Courier::where("token", $token)->first();
                    if ($courier != null) {
                        $request = $request->merge(["UserType" => 5]);
                        return $next($request);
                    } else {
                        $message = "خطا در احراز هویت پیک ";
                        return response()->json(["status" => false, "message" => $message], 403);
                    }
                    break;
                default:
                    $message = "خطا در احراز هویت  ";
                    return response()->json(["status" => false, "message" => $message], 403);
                    break;
            }

        } else {
            $message = "خطا در احراز هویت  ";
            return response()->json(["status" => false, "message" => $message], 403);
        }

    }
}
