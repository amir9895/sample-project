<?php

namespace App\Http\Controllers;

use App\Models\Marketer;
use App\Models\Seller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use IPPanel\Client;
use IPPanel\Errors\Error;
use IPPanel\Errors\HttpException;
use Kavenegar\KavenegarApi;
use Tymon\JWTAuth\Facades\JWTAuth;

class SmsController extends Controller
{

    public function CheckSendSmsData(Request $request)
    {
        $type = $request->typeSms;
        $validate = $this->SendSmsValidation($request->all());
        if ($validate->fails()) {
            $message = $validate->errors()->first();
            return response()->json(["status" => false, "message" => $message], 200);
        } else {
            $mobile = trim($request->mobile);
            $hash_id = $request->id;
//            Cache Driver => Redis
            if (Cache::get($mobile) == null) {
                if ($hash_id != null) {
                    switch ($type) {
                        case 1:
                            return $this->SendSms(2, $mobile, $hash_id);
                            break;
                        case 3:
                            return $this->SendSms(4, $mobile, $hash_id);
                            break;
                        case 5:
                            return $this->SendSms(6, $mobile, $hash_id);
                            break;
                        case 7:
                            return $this->SendSms(8, $mobile, $hash_id);
                            break;
                    }
                } else {
                    return $this->SendSms($type, $mobile);
                }
            } else {
                $error = " کد فعالسازی برای شما ارسال شده است ";
                return response()->json(['status' => false, 'message' => $error], 200);
            }

        }
    }


    public function SendSms($message_type, $phone_number, $hash_id = null, $time = null, $date = null, $user_name = null)
    {
        require_once base_path('/vendor/autoload.php');
        $code = rand(1010, 9090);
//        $code = 1111;
        $pattern = null;
        switch ($message_type) {
            case 1:
                $pattern = "Pattern";
                $patternValues = [
                    "verification-code" => (string)$code,
                ];
                break;
            case 2:
                $pattern = "Pattern";
                $patternValues = [
                    "verification-code" => (string)$code,
                    "id" => (string)$hash_id,
                ];
                break;
            case 3:
                $pattern = "Pattern";
                $patternValues = [
                    "verification-code" => (string)$code,
                ];
                break;
            case 4:
                $pattern = "Pattern";
                $patternValues = [
                    "verification-code" => (string)$code,
                    "id" => (string)$hash_id,
                ];
                break;
            case 5:
                $pattern = "Pattern";
                $patternValues = [
                    "verification-code" => (string)$code,
                ];
                break;
            case 6:
                $pattern = "Pattern";
                $patternValues = [
                    "verification-code" => (string)$code,
                    "id" => (string)$hash_id,
                ];
                break;
            case 7:
                $pattern = "Pattern";
                $patternValues = [
                    "verification-code" => (string)$code,
                ];
                break;
            case 8:
                $pattern = "Pattern";
                $patternValues = [
                    "verification-code" => (string)$code,
                    "id" => (string)$hash_id,
                ];
                break;
            case 9:
                $pattern = "Pattern";
                $patternValues = [
                    "date" => (string)$date,
                    "time" => (string)$time,
                ];
                break;
            case 10:
                // Seller Invite
                $pattern = "Pattern";
                $patternValues = [
                    "name" => (string)$user_name,
                    "link" => "https://b2n.ir/DoremonSeller",
                ];
                break;
            case 11:
                // User Invite
                $pattern = "Pattern";
                $patternValues = [
                    "name" => (string)$user_name,
                    "link" => "https://b2n.ir/Doremon",

                ];
                break;
            case 12:
                // Marketer Invite
                $pattern = "Pattern";
                $patternValues = [
                    "name" => (string)$user_name,
                    "link" => "https://b2n.ir/DoremonMarketer",

                ];
                break;
            case 13:
                //Courier Invite
                $pattern = "Pattern";
                $patternValues = [
                    "name" => (string)$user_name,
                    "link" => "https://doremoon.ir",
                ];
                break;
        }

        if ($pattern != null) {

            $apiKey = "API Key";
            $client = new Client($apiKey);
            try {

                $bulkID = $client->sendPattern(
                    $pattern,
                    "Number",
                    (string)$phone_number,
                    $patternValues
                );

                Cache::put($phone_number, $code, now()->addMinutes(1));
                $message_output = "پیامک با موفقیت ارسال شد . ";
                return response()->json(['status' => true, 'message' => $message_output], 200);

            } catch (Error $e) {
                $error = "در حال حاضر خطایی به وجود آمده است . لطفا دقایقی دیگر تلاش نمایید.";
                return response()->json(['status' => false, 'message' => $error], 200);
            }

        }
    }

    public function SendSmsValidation($request)
    {
        App::setLocale("fa");
        $validator = Validator::make($request, [
            'mobile' => 'required|string|regex:/(09)[0-9]{9}/|size:11',
            'id' => 'nullable|string|max:100'
        ]);
        return $validator;
    }

}
