<?php

namespace App\Http\Controllers;

use AloPeyk\Api\RESTful\Model\Location;
use AloPeyk\Api\RESTful\Model\Order as OrderAloPeyk;
use AloPeyk\Api\RESTful\Model\Address as AddressAloPeyk;
use App\Jobs\SendNotifications;
use App\Models\Address;
use App\Models\Brand;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Config;
use App\Models\Courier;
use App\Models\CourierPrice;
use App\Models\CourierRequests;
use App\Models\Discount;
use App\Models\Factor;
use App\Models\Favorite;
use App\Models\Marketer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Seller;
use App\Models\SellerProduct;
use App\Models\SocialNotworks;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Morilog\Jalali\CalendarUtils;
use MongoDB\BSON\UTCDateTime as MongoDate;

class UserController extends Controller
{


    public function CheckEditProfileData(Request $request)
    {
        $user = $this->CheckUserAccess($request);
        if ($user != false) {
            $validate = $this->ValidateEditUserData($request->all());
            if ($validate->fails()) {
                $message = $validate->errors()->first();
                return response()->json(["status" => false, "message" => $message], 200);
            } else {
                if (isset($request->mobile) && !empty($request->mobile)) {
                    $ExistUserMobile = User::where("mobile", $request->mobile)->first();
                    if ($ExistUserMobile != null) {
                        if ($ExistUserMobile->_id != $user->_id) {
                            $message = "کاربری دیگر با این شماره تلفن ثبت شده است";
                            return response()->json(["status" => false, "message" => $message], 200);
                        }
                    }
                }
                try {


                    if (isset($request->full_name) && !empty($request->full_name)) {
                        $user->full_name = $request->full_name;
                    }
                    if (isset($request->gender) && !empty($request->gender)) {
                        $user->gender = $request->gender;
                    }
                    if (isset($request->mobile) && !empty($request->mobile)) {
                        $user->mobile = $request->mobile;
                    }
                    if (isset($request->email) && !empty($request->email)) {
                        $user->email = $request->email;
                    }
                    if (isset($request->birthday) && !empty($request->birthday)) {
                        $user->birthday = $this->ConvertJalaliToGregorian($request->birthday);
                    }
                    if (isset($request->active_version) && !empty($request->active_version)) {
                        $user->active_version = $request->active_version;
                    }
                    if (isset($request->city_name) && !empty($request->city_name)) {
                        $user->city_name = $request->city_name;
                    }
                    if (isset($request->province_name) && !empty($request->province_name)) {
                        $user->province_name = $request->province_name;
                    }

                    if (isset($request->push_id) && !empty($request->push_id)) {
                        $user->push_id = $request->push_id;
                    }

                    $user->save();
                    $message = "پروفایل با موفقیت ویرایش شد";
                    return response()->json(["status" => true, "message" => $message], 200);

                } catch (\Exception $exception) {
                    $message = "خطایی در ویرایش پروفایل پیش آمده است";
                    return response()->json(["status" => false, "message" => $message], 200);
                }

            }
        } else {
            $message = "کاربری با این مشخصات یافت نشد ";
            return response()->json(["status" => false, "message" => $message], 403);
        }
    }

    public function UploadAvatar(Request $request)
    {
        $user = $this->CheckUserAccess($request);
        if ($user != false) {
            $validate = $this->ValidateUploadAvatarData($request->all());
            if ($validate->fails()) {
                $message = $validate->errors()->first();
                return response()->json(["status" => false, "message" => $message], 200);
            } else {
                $user_id = $user->_id;
                $image_ext = $request->file("avatar")->getClientOriginalExtension();
                $filename = $user_id . "." . $image_ext;
                $upload = $this->UploadImage($request->file("avatar"), $user_id, "users/user_avatar");
                if ($upload == true) {
                    $user->avatar = "http://pic.doremon.ir/upload/users/user_avatar/" . $filename;
                } else {
                    $message = "خطایی در اپلود تصویر پیش آمده است";
                    return response()->json(["status" => false, "message" => $message], 200);
                }
                $user->save();
                $message = "تصویر پروفایل با موفقیت ویرایش شد";
                return response()->json(["status" => true, "message" => $message], 200);
            }
        } else {
            $message = "کاربری با این مشخصات یافت نشد ";
            return response()->json(["status" => false, "message" => $message], 403);
        }
    }


    public function GetUserProfile(Request $request)
    {
        $user = $this->CheckUserAccess($request);
        if ($user != false) {
            $data = $user->toArray();
            if (isset($data["birthday"]) && $data["birthday"] != null) {
                $data["birthday"] = $this->ConvertGregorianToJalali($data["birthday"]);
            }
            $social_contact = SocialNotworks::where("type", "user")->first();
            $data["social_contact"] = $social_contact;
            return response()->json(["status" => true, "data" => $data], 200);
        } else {
            $message = "کاربری با این مشخصات یافت نشد ";
            return response()->json(["status" => false, "message" => $message], 200);
        }
    }

    public function CheckUserAccess(Request $request)
    {
        $token = $request->header("Authorization");
        $user = User::where("token", $token)->first();
        if ($user != null) {
            return $user->makeHidden("token");
        } else {
            return false;
        }
    }


    public function ValidateEditUserData($request)
    {
        App::setLocale("fa");
        $validate = Validator::make($request, [
            'full_name' => 'nullable|string|max:100',
            'mobile' => 'nullable|string|regex:/(09)[0-9]{9}/|size:11',
            'gender' => 'nullable|integer|min:1|max:2',
            'email' => 'nullable|email',
            'birthday' => 'nullable|string|size:10',
            'avatar' => 'nullable|mimes:jpeg,jpg,png|max:3000',
            'push_id' => 'nullable|string|max:30',
            'active_version' => 'nullable|string|max:30',
            'city_name' => 'nullable|string|max:300',
            'province_name' => 'nullable|string|max:300',
        ]);
        return $validate;
    }

    public function ValidateUploadAvatarData($request)
    {
        App::setLocale("fa");
        $customMessages = [
            'avatar.max' => 'حجم تصویر ارسالی بیشتر از 3 مگابایت است',
            'avatar.mimes' => 'فرمت تصویر ارسالی نامعتبر است',
            'avatar.uploaded' => 'خطایی در اپلود تصویر پیش آمده است',
        ];

        $validate = Validator::make($request, [
            'avatar' => 'required|mimes:jpeg,jpg,png|max:3000',
        ], $customMessages);
        return $validate;
    }



}
