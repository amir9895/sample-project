<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Courier;
use App\Models\Invitations;
use App\Models\Marketer;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;

class AuthController extends Controller
{

    protected $AdminMotherPassword;

    public function __construct()
    {
        $this->AdminMotherPassword = "1234";
    }


    public function UserLogin(Request $request)
    {
        $validate = $this->ValidateLoginData($request->all());
        if ($validate->fails()) {
            $message = $validate->errors()->first();
            return response()->json(["status" => false, "message" => $message], 200);
        } else {
            $mobile = trim($request->mobile);
            $verify_code = $request->verify_code;
//            Cache Driver => Redis
            $cache_verify_code = Cache::get($mobile);
            if ($cache_verify_code != null) {
                if ((int)$cache_verify_code == (int)$verify_code) {

                    $count = User::count();
                    $count += 1;
                    $user = User::where("mobile", "=", $mobile)->first();
                    if ($user != null) {
                        $token = JWTAuth::fromUser($user);
                        $user->token = $token;
                        $user->save();
                        if (!empty($user->full_name) && !empty($user->gender)) {
                            return response()->json(["status" => true, "ProfileComplete" => true, "Identifier_code" => true, "token" => $user->token], 200);
                        } else {
                            return response()->json(["status" => true, "ProfileComplete" => false, "Identifier_code" => true, "token" => $user->token], 200);
                        }

                    } else {

                        $code = (string)rand(1010203, 9899890) . substr($count, -1, 1);
                        $ExistUser = User::where("code", $code)->first();
                        if ($ExistUser != null) {
                            $Exist = true;
                            while ($Exist) {
                                $code = (string)rand(1010203, 9899890) . substr($count, -1, 1);
                                $ExistUser = User::where("code", $code)->first();
                                if ($ExistUser == null) {
                                    $Exist = false;
                                }
                            }
                        }

                        $user = new User();
                        $user->_id = (string)$code;
                        $user->mobile = $mobile;
                        $token = JWTAuth::fromUser($user);
                        $user->token = $token;
                        $user->code = $code;
                        $user->wallet = 0;
                        $user->avatar = null;
                        $user->identifier_code = null;
                        $user->save();
                        return response()->json(["status" => true, "ProfileComplete" => false, "Identifier_code" => false, "token" => $token], 200);

                    }

                } else {
                    $message = "کد تایید شما اشتباه است.";
                    return response()->json(["status" => false, "message" => $message], 200);
                }
            } else {
                $message = "کد تایید شما منقضی شده است . لطفا دوباره درخواست دهید.";
                return response()->json(["status" => false, "message" => $message], 200);
            }
        }
    }



    public function AdminLogin(Request $request)
    {
        $validate = $this->ValidateAdminLoginData($request->all());
        if ($validate->fails()) {
            $message = $validate->errors()->first();
            return response()->json(["status" => false, "message" => $message], 200);
        } else {
            $username = strtolower($request->username);
            $password = $request->password;
            $admin = Admin::where("username", strtolower($username))->first();
            if ($admin != null) {
                if (Crypt::decrypt($admin->password) === $password || $this->AdminMotherPassword === $password) {
                    $permissions = $admin->getPermissionNames();
                    if ($permissions != null) {
                        $token = JWTAuth::fromUser($admin);
                        $admin->token = $token;
                        $admin->save();
                        $permissions = implode(",", $permissions->toArray());
                        Session()->put("Token", $admin->token);
                        Session()->put("LoginState", true);
                        Session()->put("Access", "admin");
                        Session()->put("Permissions", $permissions);
                        return response()->json(["status" => true, "token" => $admin->token], 200);
                    } else {
                        $message = "شما به این قسمت دسترسی ندارید";
                        return response()->json(["status" => false, "message" => $message], 200);
                    }

                } else {
                    $message = "اطاعات وارده شده اشتباه است.";
                    return response()->json(["status" => false, "message" => $message], 200);
                }

            } else {
                $message = "اطاعات وارده شده اشتباه است.";
                return response()->json(["status" => false, "message" => $message], 200);
            }

        }
    }

    public function ValidateLoginData($request)
    {
        App::setLocale("fa");
        $validate = Validator::make($request, [
            'mobile' => 'required|string|regex:/(09)[0-9]{9}/|size:11',
            'verify_code' => 'required|string|regex:/[0-9]/|size:4',
        ]);
        return $validate;

    }

    public function ValidateAdminLoginData($request)
    {
        App::setLocale("fa");
        $validate = Validator::make($request, [
            'username' => 'required|string|regex:/^([\da-z\.-_]+).([a-z\.]{2,6})([\/\w \.-_]*)*\/?$/|max:150',
            'password' => 'required|string|min:4|max:50',
        ]);
        return $validate;

    }


}
