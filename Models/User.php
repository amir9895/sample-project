<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{

    protected $collection = "users";
    protected $fillable = ["Identifier_code"];

    public function address()
    {
        return $this->hasMany(Address::class, "user_id");
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class, "user_id");
    }

    public function carts()
    {
        return $this->belongsTo(Cart::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function invitations()
    {
        return $this->hasMany(Invitations::class, "invitor_id");
    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


}
